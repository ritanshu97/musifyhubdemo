
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:musifyhub/bindings/music_playes_binding.dart';
import 'package:musifyhub/bindings/splash_binding.dart';
import 'package:musifyhub/screens/music_player_ui.dart';
import 'package:musifyhub/utils/app_routes.dart';
import 'package:musifyhub/screens/splash_ui.dart';

class AppPages{
  static const initialRoute = AppRoutes.splash;
  static final route = [
    GetPage(
      name: AppRoutes.splash,
      page: () => const SplashUi(),
      binding: SplashBindings(),
      transition: Transition.native,
    ), GetPage(
      name: AppRoutes.musicPlayer,
      page: () => const MusicPlayerUi(),
      binding: MusicPlayerBindings(),
      transition: Transition.native,
    ),
  ];

}

