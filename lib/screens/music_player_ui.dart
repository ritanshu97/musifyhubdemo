import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MusicPlayerUi extends StatelessWidget {
  const MusicPlayerUi({super.key});
//
  //
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green.shade900,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.green.shade900,
        title: const Text('Now Playing',style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w500),),

      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                alignment: Alignment.center,
              height: Get.height*.35,
              width: Get.width*.7,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                ),
                child: const Text("song Image",style: TextStyle(color: Colors.black),),
              ),
            ).paddingOnly(bottom: Get.height*.07,top: Get.height*.05),

            Container(
              height: Get.height*.43,
              width: Get.width,
              decoration: const BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30),topRight: Radius.circular(30))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Song Name",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.white),),
                      Icon(Icons.favorite_border,color: Colors.white,size: 30,)
                    ],
                  ).paddingOnly(top: Get.height*.07),
                  const Text("Artist Name",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white),),

                  const ProgressBar(
              progress: Duration(seconds: 0),
              total:  Duration(minutes: 4),
              bufferedBarColor: Colors.white38,
              baseBarColor: Colors.white10,
              thumbColor: Colors.white,
              timeLabelTextStyle:
              TextStyle(color: Colors.white),
              progressBarColor: Colors.white,

            ).paddingOnly(top: Get.height*.08),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                          onPressed: () {

                          },
                          icon: const Icon(Icons.lyrics_outlined,
                              color: Colors.white)),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.skip_previous,
                              color: Colors.white, size: 36)),
                      IconButton(
                          onPressed: () async {

                          },
                          icon: const Icon(
                                 Icons.pause,
                            color: Colors.white,
                            size: 60,
                          )),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.skip_next,
                              color: Colors.white, size: 36)),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.loop,
                              color: Colors.white
                          )),
                    ],
                  ).paddingOnly(top: Get.height*.03)
                ],
              ).paddingOnly(left: Get.width*.05,right: Get.width*.05),
            )

          ],
        ),
      ),
    );
  }
}
