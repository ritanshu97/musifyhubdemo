import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashUi extends StatelessWidget {
  const SplashUi({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green.shade900,
      body:
          Center(
            child: Image.asset(
              "assets/images/Img_Splash.png",
              scale: 2,
              color: Colors.white,
            ),
          ),


    );
  }
}