import 'dart:developer';

import 'package:get/get.dart';
import 'package:musifyhub/utils/app_pages.dart';
import 'package:musifyhub/utils/app_routes.dart';

class SplashController extends GetxController {

  @override
  void onInit() {

    Future.delayed(const Duration(seconds: 3), () {
   Get.offAllNamed(AppRoutes.musicPlayer);
    });
    super.onInit();
  }
}
