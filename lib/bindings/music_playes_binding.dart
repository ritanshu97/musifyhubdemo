import 'package:get/get.dart';
import 'package:musifyhub/controllers/music_player_controller.dart';

class MusicPlayerBindings implements Bindings {  @override
  void dependencies() {
  Get.put(MusicPlayerController());

}
}
