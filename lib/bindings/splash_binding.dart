import 'package:get/get.dart';
import 'package:musifyhub/controllers/splash_controller.dart';

class SplashBindings implements Bindings {  @override
  void dependencies() {
  Get.put(SplashController());
  }
}
