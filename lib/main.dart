import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:musifyhub/utils/app_pages.dart';

void main() {
  runApp(const MusifyHub());
}

class MusifyHub extends StatelessWidget {
  const MusifyHub({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return
          GetMaterialApp(
            debugShowCheckedModeBanner: false,
            getPages: AppPages.route,
            initialRoute: AppPages.initialRoute,
            title: 'MusifyHub',
          );

  }
}


